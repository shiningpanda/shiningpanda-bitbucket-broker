#!/usr/bin/env python
import shiningpanda

if __name__=='__main__':
    payload = dict(
        broker='shiningpanda',
        service=dict(
            workspace='shiningpanda.org',
            job='test',
            token='?',
            parameters='foo=bar',
        ),
        commits=[
            dict(author='omansion', revision=10),
        ],
    )
    broker = shiningpanda.ShiningPanda()
    broker.handle(payload)
