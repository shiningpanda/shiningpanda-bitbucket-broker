#-*- coding: utf-8 -*-
import urllib
import unittest

import mox

import shiningpanda

class ShiningPandaTestCase(unittest.TestCase):
    
    def setUp(self):
        self.mox = mox.Mox()
        
    def tearDown(self):
        self.mox.UnsetStubs()
    
    def payload(self, **kwargs):
        service = dict(
            workspace='shiningpanda.org',
            job='pygments',
            token='PWFm8c2T',
        )
        service.update(kwargs)
        return dict(
            broker='shiningpanda',
            commits=[ dict(author='omansion', revision=10), ],
            service=service,
        )
    
    def test_handle_without_parameters(self):
        self.mox.StubOutClassWithMocks(shiningpanda, 'URLOpener')
        opener = shiningpanda.URLOpener()
        opener.open(
            'https://jenkins.shiningpanda.com/shiningpanda.org/job/pygments/build',
            urllib.urlencode(dict(
                token='PWFm8c2T',
                cause='Triggered by a push of omansion (revision: 10)',
            ))
        )
        self.mox.ReplayAll()
        s = shiningpanda.ShiningPanda()
        s.handle(self.payload())
        self.mox.VerifyAll()

    def test_handle_with_parameters(self):
        self.mox.StubOutClassWithMocks(shiningpanda, 'URLOpener')
        opener = shiningpanda.URLOpener()
        opener.open(
            'https://jenkins.shiningpanda.com/shiningpanda.org/job/pygments/buildWithParameters',
            urllib.urlencode(dict(
                token='PWFm8c2T',
                cause='Triggered by a push of omansion (revision: 10)',
                foo='bar',
            ))
        )
        self.mox.ReplayAll()
        s = shiningpanda.ShiningPanda()
        s.handle(self.payload(parameters='foo=bar'))
        self.mox.VerifyAll()

    def test_handle_strip_workspace(self):
        self.mox.StubOutClassWithMocks(shiningpanda, 'URLOpener')
        opener = shiningpanda.URLOpener()
        opener.open(
            'https://jenkins.shiningpanda.com/shiningpanda.org/job/pygments/build',
            mox.IgnoreArg(),
        )
        self.mox.ReplayAll()
        s = shiningpanda.ShiningPanda()
        s.handle(self.payload(workspace=' shiningpanda.org '))
        self.mox.VerifyAll()

    def test_handle_strip_job(self):
        self.mox.StubOutClassWithMocks(shiningpanda, 'URLOpener')
        opener = shiningpanda.URLOpener()
        opener.open(
            'https://jenkins.shiningpanda.com/shiningpanda.org/job/pygments/build',
            mox.IgnoreArg(),
        )
        self.mox.ReplayAll()
        s = shiningpanda.ShiningPanda()
        s.handle(self.payload(job=' pygments '))
        self.mox.VerifyAll()

    def test_handle_strip_token(self):
        self.mox.StubOutClassWithMocks(shiningpanda, 'URLOpener')
        opener = shiningpanda.URLOpener()
        opener.open(
            mox.IgnoreArg(),
            urllib.urlencode(dict(
                token='PWFm8c2T',
                cause='Triggered by a push of omansion (revision: 10)',
            ))
        )
        self.mox.ReplayAll()
        s = shiningpanda.ShiningPanda()
        s.handle(self.payload())
        self.mox.VerifyAll()

if __name__=='__main__':
    unittest.main()
