#-*- coding: utf-8 -*-
import urllib
import urlparse
from brokers import BaseBroker

class URLOpener(urllib.FancyURLopener):
    version = 'bitbucket.org'

class ShiningPanda(BaseBroker):

    def handle(self, payload):
        service = payload.get('service', {})
        workspace = service.get('workspace', '').strip()
        job = service.get('job', '').strip()
        token = service.get('token', '').strip()
        parameters = urlparse.parse_qs(service.get('parameters', ''))
        if not workspace    \
                or not job    \
                or not token    \
                or parameters.has_key('token')    \
                or parameters.has_key('cause'):
            return
        for values in parameters.values():
            if len(values) > 1:
                return
        query = dict(parameters)
        query['token'] = token
        commit = payload['commits'][-1]
        query['cause'] = 'Triggered by a push of %s (revision: %s)' % (commit['author'], commit['revision'])
        url = 'https://jenkins.shiningpanda.com/%s/job/%s/%s' % (workspace, job, 'buildWithParameters' if parameters else 'build')
        opener = self.get_local('opener', URLOpener)
        opener.open(url, urllib.urlencode(query, doseq=True))
